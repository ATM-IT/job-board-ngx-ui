import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './@core/@core.module';
import { AppComponent } from './app.component';

import { HomepageComponent } from './@feature/homepage/homepage.component';
import { SignupComponent } from './@feature/signup/signup.component';
import { SigninComponent } from './@feature/signin/signin.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    SignupComponent,
    SigninComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
